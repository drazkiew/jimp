#include <stdio.h>

#include "sasiedztwo.h"

#define CZARNY 0
#define BIALY 255

int sasiedztwo(pole_t kom) {
	int licz = 0;
	if (kom->tab[0][0] == CZARNY)
		licz++;
	if (kom->tab[1][0] == CZARNY)
                licz++;
	if (kom->tab[2][0] == CZARNY)
                licz++;
	if (kom->tab[0][1] == CZARNY)
                licz++;
	if (kom->tab[2][1] == CZARNY)
                licz++;
	if (kom->tab[0][2] == CZARNY)
                licz++;
	if (kom->tab[1][2] == CZARNY)
                licz++;
	if (kom->tab[2][2] == CZARNY)
                licz++;
	return licz;
}
