#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <math.h>
#include <string.h>

#include "lodepng.h"
#include "plansza.h"

void itoa(int num, char *liczba, int max);
void stworz_png_nazwany(char* folder, int i, int n, unsigned char *plansza, int width, int height, int rozmiar, char *plik, char *liczba, unsigned char *temp);
void instrukcja(void);

void instrukcja(void) {
	printf("\nOto program do symulacji zycia Johna Conwaya\nJak uruchomic:\n \"./zycie\" wraz z opcjami:\n\n");
	printf("   -p <plik_poczatkowy> - plik z zakodowana plansza poczatkowa, jezeli go nie ma zostanie wygenerowana lososwo\n\n");
	printf("      Opcje przydatne przy losowym generowaniu planszy:\n\n");
	printf("   -w <szerokosc> -h <wysokosc> - wymiary planszy poczatkowej (oba domyslnie 20)\n");
	printf("   -s <szansa> - prawdopodbienstwo utworzenia zywej komorki (od 0 do 100) (domyslnie 50)\n\n");
	printf("      Opcje ogolne:\n\n");
	printf("   -n <liczba_krokow> - liczba krokow jakie wykona program i wygeneruje obrazkow (domyslnie 20)\n");
	printf("   -f <nazwa_pliku> - adres wzgledny wygenerowanych plikow wraz z nazwa pliku (bez rozszerzenia) (domyslnie \"etapy/krok\")\n");
	printf("   -r <powiekszenie> - liczba ile razy zostana powiekszone rozmiary wygenerowanych obrazkow (domyslnie 5)\n");
	printf("   -c <czestotliwosc> - liczba krokow co ile ma zostac wygenerowany obrazek (domyslnie 1)\n");
	printf("   -z <zanikanie> - liczba krokow po jakiej zmarla komorka zniknie (efekt czysto graficzny) (domyslnie 1)\n\n");	
}

void stworz_png_nazwany(char* folder, int i, int n, unsigned char *plansza, int width, int height, int rozmiar, char *plik, char *liczba, unsigned char *temp) {
        strcpy(plik, folder);

        itoa(i, liczba, n);

        strcat(plik, liczba);
        strcat(plik, ".png");

        stworz_png( plik, plansza, width, height, rozmiar, temp);
}

void itoa(int num, char *liczba, int max) {
	int l = log10(max);
	char *tekst;
	int i, b;	
	int a = 0;
	tekst = malloc((l + 2) * sizeof *tekst);
	
	for (i = 0; i <= l; i++) {
		b = 10 * a;
		a = (int) (num / pow(10, l - i));
		tekst[i] = a - b + '0';
	}
	tekst[i] = '\0';
	strcpy(liczba, tekst);
}

int main (int argc, char **argv) {
	int opt, i, spr;
	char *folder = NULL;
	char *inp = NULL;
	int width = 20;
	int height = 20;
	int szansa = 50;
	int rozmiar = 5;
	int czestosc = 1;
	int zanik = 1;
	FILE *in;

	char *plik;
	char *liczba;
	int *pamiec;
	unsigned char *temp;
	unsigned char *plansza;	

	int n = 20;

	if (argc == 1) { 
		instrukcja();
                return 1;
	}

  	while ((opt = getopt (argc, argv, "p:w:h:s:n:f:r:c:z:")) != -1) {
    		switch (opt) {
    		case 'p':
			inp = optarg;
      			break;
		case 'w':
			width = atoi(optarg);
			if (width < 1)
                                width = 1;
			break;
		case 'h':
			height = atoi(optarg);
			if (height < 1)
                                height = 1;
			break;
		case 'n':
			n = atoi(optarg);
			if (n < 1)
				n = 1;
			break;
		case 's':
			szansa = atoi(optarg);
			if (szansa < 0)
				szansa = 0;
			if (szansa > 100)
				szansa = 100;
			break;
		case 'f':
			folder = optarg;
			break;
		case 'r':
			rozmiar = atoi(optarg);
			if (rozmiar < 1)
                                rozmiar = 1;
			break;
		case 'c':
			czestosc = atoi(optarg);
			if (czestosc < 1)
				czestosc = 1;
			break;
		case 'z':
			zanik = atoi(optarg);
			if (zanik < 1)
				zanik = 1;
			break;
     		default:
			return 1;
    		}
  	}		
	if( optind < argc ) {
		printf("\nBledne parametry!\n");
		for( ; optind < argc; optind++ )
			printf("\t\"%s\"\n", argv[optind] );
		printf("\n" );
		instrukcja();
		return 1;
	}
	if (folder == NULL)
		folder = "etapy/krok";

	if (inp == NULL) {
		plansza = malloc(4 * width * height * sizeof *plansza);	
		losuj_plansze(szansa, plansza, width, height);
	} else {
		in = fopen(inp, "r");
		wymiary(in, &width, &height);
		plansza = malloc(4 * width * height * sizeof *plansza);	
		if ((spr = odczytanie_planszy(in, plansza, width, height)) != 0) {
			printf("\nPlik \"%s\" jest zle sformatowany!\n", inp);
			if (spr == 1)
				printf("Podane w pliku wymiary sa za male\n\n");
			else
				printf("Podane w pliku wymiary sa za duze\n\n");
			return 1;
		}
	}

	plik = malloc((strlen(folder) + log10(n) + 4) * sizeof *plik);
        liczba = malloc((log10(n) + 2) * sizeof *liczba);
	pamiec = malloc(3 * width * height * sizeof *pamiec);
	temp = malloc(4 * width * height * rozmiar * rozmiar * sizeof *pamiec);

	stworz_png_nazwany( folder, 0, n, plansza, width, height, rozmiar, plik, liczba, temp);	

	for (i = 1; i <= n; i++) {
		if (aktualizacja_planszy(plansza, width, height, zanik, pamiec) == 1) {
			printf("Rozwoj planszy zatrzymal sie miedzy krokiem nr.%i, a nr.%i (dalsze zmiany sa niemozliwe)\n"
				"Dalsza praca jest niepotrzebna, program wstrzymany\n", i - 1, i);
			return 0;
		}
		if (i % czestosc == 0)
			stworz_png_nazwany( folder, i, n, plansza, width, height, rozmiar, plik, liczba, temp);		
	}
	return 0;	
}

