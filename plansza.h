#ifndef PLANSZA_H
#define PLANSZA_H

int odczytanie_planszy(FILE *in, unsigned char *plansza, int w, int h);

void wymiary(FILE *in, int *w, int *h);

int aktualizacja_planszy(unsigned char *plansza, int width, int height, int zanik, int *pamiec);

void losuj_plansze(int szansa, unsigned char *image, int width, int height);

#endif
