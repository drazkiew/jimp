#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define CZARNY 0
#define BIALY 255 

#include "plansza.h"
#include "sasiedztwo.h"

int aktualizacja_planszy(unsigned char *plansza, int width, int height, int zanik, int *pamiec) {
	int x, y, licz, ix, iy, i;
	unsigned char wart;
	int ktory = 0;	
	pole_t kom = malloc(sizeof *kom);	

	for (x = 0; x < width; x++)
	for (y = 0; y < height; y++) {
		licz = 0;
		wart = plansza[4 * width * y + 4 * x]; 		

		for (ix = x - 1; ix <= x + 1; ix++)
		for (iy = y - 1; iy <= y + 1; iy++)
                	if (ix >= 0 && ix < width && iy >= 0 && iy < height)
				kom->tab[ix - x + 1][iy - y + 1] = plansza[4 * width * iy + 4 * ix];
			else
				kom->tab[ix - x + 1][iy - y + 1] = BIALY;

		licz = sasiedztwo(kom);

		if (wart == CZARNY) {
			if (licz < 2 || licz > 3) {
				pamiec[ktory * 3 + 0] = x;
				pamiec[ktory * 3 + 1] = y;
				pamiec[ktory * 3 + 2] = 1;
				ktory++;
			}
		} else {
                        if (licz == 3) {
                                pamiec[ktory * 3 + 0] = x;
                                pamiec[ktory * 3 + 1] = y;
                                pamiec[ktory * 3 + 2] = 2;
                                ktory++;
                        } else if (wart != BIALY) {
				wart += (unsigned char) BIALY / zanik;
				if (wart <= BIALY) {
					plansza[4 * width * y + 4 * x + 0] = wart;
					plansza[4 * width * y + 4 * x + 1] = wart;
					plansza[4 * width * y + 4 * x + 2] = wart;		
				} else {
					plansza[4 * width * y + 4 * x + 0] = (unsigned char) BIALY;
					plansza[4 * width * y + 4 * x + 1] = (unsigned char) BIALY;
					plansza[4 * width * y + 4 * x + 2] = (unsigned char) BIALY;
				}
			}
                }
	}	
	if (ktory == 0) 
		return 1;
	for (i = 0; i < ktory; i++) {
		x = pamiec[i * 3 + 0];
		y = pamiec[i * 3 + 1];
		
		if (pamiec[i * 3 + 2] == 1) {
			wart = (unsigned char) BIALY / zanik;
			plansza[4 * width * y + 4 * x + 0] = wart;
			plansza[4 * width * y + 4 * x + 1] = wart;
			plansza[4 * width * y + 4 * x + 2] = wart;
		}
		if (pamiec[i * 3 + 2] == 2) {
                        plansza[4 * width * y + 4 * x + 0] = (unsigned char) CZARNY;
                        plansza[4 * width * y + 4 * x + 1] = (unsigned char) CZARNY;
                        plansza[4 * width * y + 4 * x + 2] = (unsigned char) CZARNY;
                }
		pamiec[i * 3 + 0] = 0;;
		pamiec[i * 3 + 1] = 0;;
		pamiec[i * 3 + 2] = 0;;	
	}
	return 0;
}

int odczytanie_planszy(FILE *in, unsigned char *plansza, int w, int h) {
	
	int i;
	char c;	
	
	i = 0;
	do {
		c = fgetc(in);

		if ((c == '-' || c == '0') && i >= w * h) {
                        fclose(in);
                        return 1;
                }

		if (c == '0') {
			plansza[4 * i + 0] = (unsigned char) BIALY;
			plansza[4 * i + 1] = (unsigned char) BIALY;
			plansza[4 * i + 2] = (unsigned char) BIALY;
			plansza[4 * i + 3] = (unsigned char) 255;
		}
		if (c == '-') {
                        plansza[4 * i + 0] = (unsigned char) CZARNY;
                        plansza[4 * i + 1] = (unsigned char) CZARNY;
                        plansza[4 * i + 2] = (unsigned char) CZARNY;
                        plansza[4 * i + 3] = (unsigned char) 255;
                }	
		if (c == '-' || c == '0')
			i++;
	
	} while (c != EOF);
	fclose(in);

	if (i < w * h) {
		return 2;
	}

	return 0;
}

void wymiary(FILE *in, int *w, int *h) {
        fscanf(in, "%i %i", w, h);
}

void losuj_plansze(int szansa, unsigned char *image, int width, int height)
{
  int x, y, stan;

  srand(time(NULL));

  for(y = 0; y < height; y++)
  for(x = 0; x < width; x++)
  {
    stan = (int)(rand() / (RAND_MAX + 1.0) * 100.0) > szansa ? BIALY : CZARNY;
    image[4 * width * y + 4 * x + 0] = (char) stan;
    image[4 * width * y + 4 * x + 1] = (char) stan;
    image[4 * width * y + 4 * x + 2] = (char) stan;
    image[4 * width * y + 4 * x + 3] = (char) 255;
  }
}

