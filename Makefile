CFLAGS = -Wall -ansi -pedantic

all: zycie.o lodepng.o lodepng.h plansza.o plansza.h sasiedztwo.o sasiedztwo.h
	$(CC) $(CFLAGS) -o zycie zycie.o lodepng.o plansza.o sasiedztwo.o -lm

zycie.o: zycie.c
	$(CC) $(CFLAGS) -c zycie.c -lm

lodepng.o: lodepng.c lodepng.h
	$(CC) $(CFLAGS) -c lodepng.c

plansza.o: plansza.c plansza.h
	$(CC) $(CFLAGS) -c plansza.c

sasiedztwo.o: sasiedztwo.c sasiedztwo.h
	$(CC) $(CFLAGS) -c sasiedztwo.c

.PHONY: clrpng clrpliki pliki

pliki: clrpliki all

clrpliki:
	-rm *.o zycie 
clrpng:
	-rm etapy/*.png


